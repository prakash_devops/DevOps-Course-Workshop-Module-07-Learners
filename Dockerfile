FROM mcr.microsoft.com/dotnet/sdk:6.0
WORKDIR /app/Module_8/
RUN curl -fsSL https://deb.nodesource.com/setup_16.x | bash -
RUN apt-get install -y nodejs
COPY ./DotnetTemplate.Web/ .
RUN dotnet build
RUN npm install
RUN npm run build
ENTRYPOINT ["dotnet", "run"]
